#ifndef _DATE_HXX
#define _DATE_HXX
#include<iostream>
#include<string>

class date
{
  private: 
    int _day;
    int _mounth;
    int _year;

  public:
    date()=default; // default konstuktor
    date(int day, int mounth, int year); // konstruktor koji prihvata 3 int varijable i setuje privatne clanove
    date(std::string fullDate);// kontruktor koji uzme datum oblika dd.mm.yy i podijeli na intove kako bi setovo clanove
    date(const date& second); // copy konstuktor
    date(date&& second); // move konsturkor
    ~date()=default; // destruktor
   date& operator=(const date& second); // operator dodjeljivanja vrijednosti
   date& operator=(date&& second); // move operator dodjeljivanja vrijednosti

   void setDay(int d){_day = d;} // podesi dan
   void setMounth(int m){_mounth = m;} // podesi mjesec
   void setYear(int y){_year = y;} // podesi godinu
   const int& getDay() const {return _day;} // vrati dan
   const int& getMounth() const {return _mounth;} // vrati mjesec
   const int& getYear() const { return _year;} // vrati godinu

   const bool operator<(const date& b) const; // A<B
   const bool operator>(const date& b) const; // A>B
   const bool operator==(const date& b) const; // A==B dan == dan && mjesec == mjesec  && godina == godina
   const bool operator!=(const date& b) const; // A!=B dan != dan || mjesec != mjesec || godina != godina

   friend std::ostream& operator<<(std::ostream& out, const date& other); // formatiran ispis dd.mm.yy
   friend std::istream& operator>>(std::istream& in, date& other); // unos datuma, napravljene provjere za neispravan unos
};
std::ostream& operator<<(std::ostream& out, const date& other)
{
  if(other._day<10)
  {
    out<<"0"<<other._day<<".";
  }
  else
  {
    out<<other._day<<".";
  }
  if(other._mounth<10)
  {
    out<<"0"<<other._mounth<<".";
  }
  else
  {
    out<<other._mounth<<".";
  }
  out<<other._year;

  return out;

}
std::istream& operator>>(std::istream& in, date& other)
{
  int day, mounth, year;

  do
  {
    std::cout <<"Unesite datum[DAN MJESEC GODINA]: ";
    in>>day>>mounth>>year;

    if(day<1 || day>31 || mounth <1 || mounth >12 || year <1750 || year > 2100)
    {
      std::cout <<"Pogresno unesen datum! Ponovite unos!"  << std::endl;
    }
    else
    {
      other._day = day;
      other._mounth = mounth;
      other._year = year;
    }
  }while(day<1 || day>31 || mounth <1 || mounth >12 || year <1750 || year > 2100);

  return in;
}
date::date(int day, int mounth, int year):_day(day), _mounth(mounth), _year(year){}
date::date(std::string fullDate)
{
  std::string day,mounth,year;

  day = fullDate.substr(0,2);
  mounth = fullDate.substr(3,2);
  _year = atoi(fullDate.substr(6,4).c_str());

  if(day[0]=='0')
  {
    _day = atoi(day.substr(1).c_str());
  }
  else
  {
    _day =atoi(day.c_str());
  }

  if(mounth[0]=='0')
  {
    _mounth = atoi(mounth.substr(1).c_str());
  }
  else
  {
    _mounth = atoi(mounth.c_str());
  }
 }
date::date(const date& second):_day(second._day), _mounth(second._mounth), _year(second._year){}
date::date(date&& second):_day(second._day), _mounth(second._mounth), _year(second._year)
{
  second._day = 0;
  second._mounth = 0;
  second._year = 0;
}
date& date::operator=(const date& second)
{
  if(this != &second)
  {
    _day = second._day;
    _mounth = second._mounth;
    _year = second._year;
  }
  return *this;
}
date& date::operator=(date&& second)
{
    _day = second._day;
    _mounth = second._mounth;
    _year = second._year;
    second._day = 0;
    second._year = 0;
    second._mounth = 0;
    return *this;
}


const bool date::operator<(const date& b) const
{
  if(_year < b._year)
  {
    return true;
  }
  else if (_year == b._year)
  {
    if(_mounth < b._mounth)
    {
      return true;
    }
    else if(_mounth == b._mounth)
    {
      if(_day < b._day)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}
const bool date::operator>(const date& b) const
{
 if(_year > b._year)
  {
    return true;
  }
  else if (_year == b._year)
  {
    if(_mounth > b._mounth)
    {
      return true;
    }
    else if(_mounth == b._mounth)
    {
      if(_day > b._day)
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}
const bool date::operator==(const date& b) const
{
  return((_day == b._day) && (_mounth == b._mounth) && (_year == b._year));
}
const bool date::operator!=(const date& b) const
{
  return((_day != b._day) || (_mounth != b._mounth) || (_year != b._year));
  
}
#endif
