#ifndef _CONSTITERATOR_HXX
#define _CONSTITERATOR_HXX
#include<iterator>
#include"node1.hxx"
#include"iterator.hxx"
template<typename Type>
class iterator;
template<typename Type>
class const_iterator : public std::iterator<std::forward_iterator_tag, Type>
{
  private:
   nodeType<Type> *current;

public:
   nodeType<Type>* getPtr() const {return current;}
   typedef Type value_type;
   typedef Type& reference;
   typedef Type* pointer;
   typedef int difference_type;
   typedef std::forward_iterator_tag iterator_category;

   const_iterator();
   const_iterator(nodeType<Type>* ptr);
   const_iterator(iterator<Type> other);
   const_iterator<Type>& operator=(const const_iterator<Type>& right);

   const_iterator<Type>& operator=( iterator<Type>& right);
   const_iterator<Type>& operator++();
   const_iterator<Type> operator++(int);
   reference operator*() const; 
   pointer operator->() const; 
   bool operator==(const const_iterator<Type>& right);
   bool operator!=(const const_iterator<Type>& right);
   
};

template<typename Type>
const_iterator<Type>::const_iterator()
{
  current = nullptr;
}

template<typename Type>
const_iterator<Type>::const_iterator(nodeType<Type>* ptr)
{
  current = ptr;
}

template<typename Type>
const_iterator<Type>::const_iterator(iterator<Type> other)
{
  current = other.getPtr();
}

template<typename Type>
const_iterator<Type>& const_iterator<Type>::operator=(const const_iterator<Type>& right)
{
  current = right.current;

  return *this;
}

template<typename Type>
const_iterator<Type>& const_iterator<Type>::operator=( iterator<Type>& right)
{
  current = right.getPtr();

  return *this;
}

template<typename Type>
const_iterator<Type>& const_iterator<Type>::operator++()
{
  current = current->getNext();

  return *this;
}

template<typename Type>
const_iterator<Type> const_iterator<Type>::operator++(int)
{
  const_iterator<Type> temp(*this);
  current = current->getNext();
  return temp;
}

template<typename Type>
Type& const_iterator<Type>::operator*() const
{
  return current->getInfo();
}

template<typename Type>
Type* const_iterator<Type>::operator->() const
{
  return current;
}

template<typename Type>
bool const_iterator<Type>::operator==(const const_iterator<Type>& right)
{
  return (current == right.current);
}


template<typename Type>
bool const_iterator<Type>::operator!=(const const_iterator<Type>& right)
{
  return (current != right.current);
}
#endif
