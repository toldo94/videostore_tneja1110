#include"movie.h"
#include"priority_queue.h"

class movie_store : public priority_queue<movie>{
  public:
    movie_store()=default;
    int search(string)const;//pretrazuje film po imenu
    void search()const;//pretrazivanje videoteke
    bool search(int,string)const;//pretrazuje film po godini izdanja i imenu
    int checkNumOfFilms(string)const;
    void print_movie()const;//ispis podataka za film
    void print()const;//ispisuje sve filmove
    bool deleteMovie(string,int);
    void deleteMovie();//brise film
    void addNewMovie();//dodavanje filma
    bool movieBack(string& str);
    void movieBack(int,string);//vracanje filma
    bool loanMovie(string&);//posudivanje filma
    bool loan(int,string);
    ofstream& write(ofstream&);//ispisivanje u datoteku
    void saveDatatoFile();//ucitavanje u datoteku
    void updateMovieStore(int);//azuriranje filmova
    void bindString(int,string&,string);//spaja film+"("+godina+")"
    ifstream& read(ifstream&);//ucitavanje iz datoteke 
    ~movie_store()=default;
};
void movie_store::print()const//ispis videoteke
{
std::cout <<std::string(10,'*')<<" Videoteka videostore_tneja1110 "<< std::string(10,'*')<< std::endl;
    priority_queue<movie> pq(*this);
    while (!pq.empty()){
      std::cout << pq.top();  
      pq.pop();
    }
}
void movie_store::updateMovieStore(int num){//azuriranje filmova
switch(num)
  case 1:
  {
      int temp,relaseDate;
      string movie_name;
      cout<<"Unesite naziv filma"<<endl;
      cin.ignore();
      getline(cin,movie_name);
      cout<<"Unesite godinu izdavanja zeljenog filma"<<endl;
      cin>>relaseDate;
      cin.ignore();
      cout<<"Unesite broj kopija"<<endl;
      cin>>temp;
      for(int i=0;i<this->size();++i){
        if(((*((this->getPointer())+i)).getRelaseDate()==relaseDate)&&((*((this->getPointer())+i)).getName()==movie_name)){
          if(temp>-1)
          (*((this->getPointer())+i)).set_numberOfCopies(temp);
          else
            break;
          }
        } 
  }
}

void movie_store::movieBack(int relaseDate,string movie_name){//vracanje jedinstvenog filma
  for(int i=0;i<this->size();++i){
    if(((*((this->getPointer())+i)).getRelaseDate()==relaseDate)&&((*((this->getPointer())+i)).getName()==movie_name)){
      (*((this->getPointer())+i)).set_numberOfCopies();
     }
  }
}

int movie_store::checkNumOfFilms(string movie_name)const{//vraca broj filmova s istim imenom
  int num=0;
  for(int i=0;i<this->size();++i){
    if((*((this->getPointer())+i)).getName()==movie_name){
       num++;
     }
  }
  return num;
}

bool movie_store::movieBack(string& str){//vracanje filma
      int temp,num;
      string movie_name;
      cout<<"Unesite naziv filma kojeg zelite vratiti"<<endl;
      cin.ignore();
      getline(cin,movie_name);
      temp=this->checkNumOfFilms(movie_name);
      if(!temp){
        cout<<"Nema filma s imenom:"<<movie_name<<endl;
        return false;
      }
      else{
        cout<<"Unesite godinu izdavanja zeljenog filma"<<endl;
        cin>>num;
        cin.ignore();
        this->movieBack(num,movie_name);
        bindString(num,str,movie_name);//string iz main funkcije
        return true;
      }

}
bool movie_store::loan(int relaseDate,string movie_name){//posudivanje jedinstvenog filma
  for(int i=0;i<this->size();++i){
    if(((*((this->getPointer())+i)).getRelaseDate()==relaseDate)&&((*((this->getPointer())+i)).getName()==movie_name)){ 
      if((*((this->getPointer())+i)).getNumberOfCopies()==0){
        std::cout<<"Nema vise filmova s imenom:"<<movie_name<<std::endl;
        return false;
      }
      (*((this->getPointer())+i)).unSet_numberOfCopies();
     }
  }
  return true;
}
bool movie_store::loanMovie(string& str){//posudivanje filma 
      int temp,num;
      string movie_name;
      cout<<"Unesite naziv filma kojeg zelite posuditi"<<endl;
      cin.ignore();
      getline(cin,movie_name);
      temp=this->search(movie_name);
      if(!temp){
        cout<<"Nema filma s imenom"<<movie_name<<endl;
        return false;
      }
      else{
        cout<<"Unesite godinu izdavanja zeljenog filma"<<endl;
        cin>>num;
        cin.ignore();
        if(!this->loan(num,movie_name))return false;
        bindString(num,str,movie_name);
        return true;
      }

}
//sastavlja string ImeFilma + "(" + godinaizdanja + ")"
void movie_store::bindString(int num,string& str,string movie_name){ 
  str=movie_name;
  str=str+"(";
  str=str+to_string(num);
  str=str+")";
  
}

void movie_store::saveDatatoFile(){//ucitavanje podataka u datoteku
      ofstream out("movie_store.txt");
      this->write(out);
      out.close();
}
void movie_store::deleteMovie(){//brisanje filma
  print();
  string movie_name;
  int num;
  cin.ignore();
  cout<<"unesite naziv filma kojeg zelite obrisat"<<endl;
  getline(cin,movie_name);//ako unesemo prazan karakter nakon imena ne radi
  cout<<"unesite godinu izdanja filma kojeg zelite obrisat"<<endl;
  cin>>num;
  cin.ignore();
  if(this->deleteMovie(movie_name,num))
    cout<<"Film:"<<movie_name<<" uspjesno izbrisan"<<endl;
  else
    cout<<"Film:"<<movie_name<<" ne postoji u videoteci"<<endl;
}
bool movie_store::deleteMovie(string movie_name,int relaseDate){//brisanje jedinstvenog filma 
  bool t=false; 
  movie_store temp;
  int num=this->size();
  for(int i=0;i<num;++i){
    if(((*((this->getPointer())+i)).getName()==movie_name)&&((*((this->getPointer())+i)).getRelaseDate()==relaseDate)){//ispitiva film kod kojeg se poklapa ime i godina izdanja
      t=true;
    }
    else 
      temp.push(*((this->getPointer())+i));
  }
  *this=move(temp);
  return t;
}


void movie_store::addNewMovie(){//dodavanje filma
  std::string movie_name;
  std::string description;
  movieStudio studio;
  int relaseDate;
  int numOfCopies;
  cin.ignore();
  cout<<"Unesite ime filma"<<endl;
  getline(cin,movie_name);
  std::cout<<"Unesite deskripciju filma"<<std::endl;
  getline(cin,description); 
  cin>>studio;
  std::cout<<"Unesite godine izdanja filma"<<std::endl;
  cin>>relaseDate;
  cin.ignore();
  cout<<"Uneiste broj kopija"<<endl;
  cin>>numOfCopies;
  cin.ignore();
  movie f1(movie_name,description,studio,relaseDate,numOfCopies);  
  this->push(f1); 
}
void movie_store::print_movie()const{//ispiuje jedinstveni film
      int num;
      std::string movie_name;
      std::cout<<"Unesite naziv filma"<<endl;
      cin.ignore();
      getline(cin,movie_name);
      std::cout<<"Unesite godinu izdanja filma"<<endl;
      cin>>num;
      cin.ignore();
      if(!this->search(num,movie_name))cout<<"Nema filma s imenom:"<<movie_name<<endl;
}
int movie_store::search(string movie_name)const{//pretrazivanje po imenui vracanje broj filmova s istim imenom
  movie m;
  int num=0;
  for(int i=0;i<this->size();++i){
    if((*((this->getPointer())+i)).getName()==movie_name){
       m=(*((this->getPointer())+i));
       cout<<m;
       num++;
     }
  }
  return num;
}

void movie_store::search()const{//pretrazivanje videoteke
      int temp,num;
      string movie_name;
      cout<<"Unesite naziv filma kojeg zelite pretraziti"<<endl;
      cin.ignore();
      getline(cin,movie_name);
      temp=this->search(movie_name);
      if(!temp)cout<<"Nema filma s imenom:"<<movie_name<<endl;
      else if(temp!=1){
        cout<<"Unesite godinu izdavanja zeljenog filma"<<endl;
        cin>>num;
        cin.ignore();
        this->search(num,movie_name);
      }
}
//pretrazivanje po imenu i godini izdanja.Ispis jedinstvenog filma
bool movie_store::search(int relaseDate,string movie_name)const{
  movie m;
  for(int i=0;i<this->size();++i){
    if(((*((this->getPointer())+i)).getRelaseDate()==relaseDate)&&((*((this->getPointer())+i)).getName()==movie_name)){
       m=(*((this->getPointer())+i));
       cout<<m;
       return true;
     }
  }
  return false;
}

ofstream& movie_store::write(ofstream& out)//funkcija za ispis u datoteku
{
    priority_queue<movie> pq(*this);
    while (!pq.empty()){
      out << pq.top();  
      pq.pop();
    }
    return out;  
}

ifstream& movie_store::read(ifstream& in){//ucitavanje iz datoteke
  while (true){
  movie temp_movie;
  in >> temp_movie;
  if(in.eof())break;
  this->push(temp_movie);
  }
  return in;
}

