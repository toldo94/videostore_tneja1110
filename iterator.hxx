#ifndef _ITERATOR_HXX
#define _ITERATOR_HXX
#include "node1.hxx"
#include "constIterator.hxx"
#include <iterator>
template<typename Type>
class const_iterator;
template<typename Type>
class iterator: public std::iterator<std::forward_iterator_tag, Type>
{
  private:
  nodeType<Type>* current;
  public:
  nodeType<Type>* getPtr(){return current;}
  typedef Type value_type;
  typedef Type& reference;
  typedef Type* pointer;
  typedef int difference_type;
  typedef std::forward_iterator_tag iterator_category;
  iterator();
  iterator(nodeType<Type>* ptr);
  iterator(const_iterator<Type>& other);
  iterator<Type>& operator=(iterator<Type>& right);
  iterator<Type>& operator=(const const_iterator<Type>& right);
  iterator<Type>& operator++();
  iterator<Type> operator++(int);
  reference operator*();
  pointer operator->() const ;
  bool operator==(const iterator<Type>& right) const;
  bool operator!=(const iterator<Type>& right) const;
};
template<typename Type>
iterator<Type>::iterator(const_iterator<Type>& other)
{
  current = other.getPtr();
}

template<typename Type>
iterator<Type>::iterator()
{
  current = nullptr;
}

template<typename Type>
iterator<Type>::iterator(nodeType<Type>* ptr)
{
  current = ptr;
}

template<typename Type>
iterator<Type>& iterator<Type>::operator=(iterator<Type>& right)
{
  current = right.current;

  return *this;
}

template<typename Type>
iterator<Type>& iterator<Type>::operator=(const const_iterator<Type>& right)
{
  current = right.getPtr();

  return *this;
}


template<typename Type>
iterator<Type>& iterator<Type>::operator++()
{
  current = current->getNext();
  
  return *this;
}

template<typename Type>
iterator<Type> iterator<Type>::operator++(int)
{
  iterator<Type> temp(*this);
  current = current->getNext();
  return temp;
}

template<typename Type>
Type& iterator<Type>::operator*()
{
  return current->getInfo();
}

template<typename Type>
Type* iterator<Type>::operator->() const
{
  return current;
}

template<typename Type>
bool iterator<Type>::operator==(const iterator<Type>& right) const
{
  return (current == right.current);
}

template<typename Type>
bool iterator<Type>::operator!=(const iterator<Type>& right) const
{
  return (current != right.current);
}

#endif
