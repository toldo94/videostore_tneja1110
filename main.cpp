#include<iostream>
#include<termios.h>
#include<unistd.h>
#include<stdio.h>
#include<string>
#include"name.hxx"
#include"date.hxx"
#include"user.hxx"
#include"binarySearchTree.hxx"
#include"queue.hxx"
#include"movie_store.h"
bool isAdmin(const std::string uN, const std::string pW);//provjerava da li je prijavljeni korisnik admin
void printAdminWllcomeMessage(const std::string username); //Admin poruka dobrodošlice
int adminMenu(); //opcije za admina
int userMenu(); //opcije za korisnika
int updateMenu(); //opcije za izmjenu podataka o korisniku
int updateMovie(); //opcije za izmjenu podataka o filmu DODANO
queue<std::string> StringSplit(std::string line, const char delimiter = ',');
void loadData(BstTree& users);//ucitavanje iz datoteke
void loadData(movie_store& movie);//DODANO
int getch();//sakriti pass
std::string getpass(const char *prompt,bool show_asterisk = true); //dobijanje passworda sakrivenog
int main()
{

  std::string username;
  BstTree users;
  movie_store m_store;
  loadData(users);
  loadData(m_store);
  std::cout <<"Unesite useraname: ";
  std::cin >> username;
  std::cin.ignore();
  std::string password=getpass("Unesite password: ",true);
  if(isAdmin(username,password))
  {
    int izbor;
    printAdminWllcomeMessage(username);
    while((izbor = adminMenu())!=14)
    switch(izbor)
    {
      case 1://pregled sadrzaja videoteke
        {
          m_store.print();
          break;
        }
      case 2://pretraga videoteke
        {
          m_store.search(); 
          break;
        }
      case 3://ispis svih podataka za odabrani film
        { 
          m_store.print_movie();
          break;
        }
      case 4://dodavanje novog filma
        { 
          m_store.addNewMovie();
          break;
        }
      case 5://brisanje odabranog filma
        { 
          m_store.deleteMovie();
          break;
        }
      case 6://azuriranje postojecih filmova
        { 
          m_store.updateMovieStore(updateMovie());
          break;
        }
      case 7://pregled liste korisnika.ispis po datumu najnoviji prvi
        {
          users.printAllUsers();//kako ispisuje od najveceg???
        break;
        }
      case 8://pretraga liste korisnika imenu prezimenu korisnickom imenu
        {
         users.searchForUser();
        break;
        }
      case 9://dodavanje novog korisnika
        {
        users.addNewUser();
       break;
        }
      case 10://brisanje odabranog korisnika
        {
       users.deleteSpecificUser();
        break;
        }
      case 11://azuriranje postojecih korsnika
        {
          users.updateUser(updateMenu());
        break;
        }
      case 12://pregled posudjenih filmova za odabranog korisnika
        {
          std::string username;
          std::cout << "Unesite username: ";
          std::cin >> username;
          if(users.isUserThrere(username))
          {
            user current;
            current = users.searchSpecificUser(username);
            current.printLoanedMovies();
          }
          else
          {
            std::cout <<"Kakav korisnik ne postoji"<< std::endl;
          }
          
          break;
        }
      case 13://pregled historije posudivanja za odabranog korisnika
        {
          std::string username;
          std::cout << "Unesite username: ";
          std::cin >> username;
          if(users.isUserThrere(username))
          {
            user current;
            current = users.searchSpecificUser(username);
            current.printUserHistory();
          }
          else
          {
            std::cout <<"Kakav korisnik ne postoji"<< std::endl;
          }
        break;
        }

      default:
        {
          std::cout << "Takva opcija ne postoji!" << std::endl;
        }
    }
  }
  else if(users.isUserCorrect(username,password))
  {
   int izbor;
   user currentUser = users.searchSpecificUser(username);
   std::cout << currentUser.getFirstName()<<", doblodošli u videoteku videostore_tneja1110" << std::endl;
   while((izbor=userMenu()) != 8)
   {
     switch(izbor)
     {
       case 1://pregleda sadrzaja vidoeteke sortiran
         {
         m_store.print();
         break;
         }
       case 2://pretraga sadzaja videoteke po nazivu filma
         {
           m_store.search();
           break;
         }
       case 3://ispis svih podataka za odabrani film
         {
           m_store.print_movie();
           break;
         }
       case 4://posudivanje filma
         {
           string str; 
           if(m_store.loanMovie(str)){
             currentUser.loanMovie(str);
           }
           break;
         }
         
       case 5://vracanje filma
         { 
           string str;
           if(m_store.movieBack(str)){
             currentUser.returnMovie(str);//punjenje historije posudivanja
           }
           break;
         }
       case 6://Pregled svoje historije posuđivanja filmova 
         {
           currentUser.printUserHistory();
           break;
         }
         
       case 7://Pregled trenutne liste posuđenih filmova
         {
           currentUser.printLoanedMovies();
           break;
         }
       default://izlaz iz programa
         {
           std::cout <<"Takva opcija ne postoji"  << std::endl;
         }
     }
   }
   users.updateCurrentUser(currentUser);
  }
  else
  {
    std::cout <<"Takav korisnik nije uclanjen u videoteku!"  << std::endl;
  }
 
  users.saveDatatoFile();
  m_store.saveDatatoFile();

return 0;
}
bool isAdmin(const std::string uN, const std::string pW)
{
  return ((uN == "nermin" && pW == "71938246") || (uN == "jakub" && pW == "tnejaJakub" ));
}
void printAdminWllcomeMessage(const std::string username)
{
  if(username == "jakub")
  {
    std::cout << "Jakub, dobrodosli u videoteku  videostore_tneja1110" << std::endl;
  }
  else
  {
    std::cout << "Nermin, dobrodosli u videoteku videostore_tneja1110" << std::endl;
  }
}
int adminMenu()
{
  int izbor;
  std::cout <<std::string(10,'*')<<"Admin opcije"<<std::string(10,'*') << std::endl;
  std::cout << " 1 -> Pregled sadrzaja videoteke" << std::endl;
  std::cout << " 2 -> Pretraga sadrzaja videoteke po nazivu filma" << std::endl;
  std::cout << " 3 -> Ispis svih podataka za odabrani film" << std::endl;
  std::cout << " 4 -> Dodavanje novog filma u videoteku" << std::endl;
  std::cout << " 5 -> Brisanje odabranog filma iz videoteke"<< std::endl;
  std::cout << " 6 -> Azuriranje postojecih filmova" << std::endl;
  std::cout << " 7 -> Pregled liste korisnika" << std::endl;
  std::cout << " 8 -> Pretraga liste korisnika" << std::endl;
  std::cout << " 9 -> Dodavanje novog korisnika" << std::endl;
  std::cout << "10 -> Brisanje odabranog korisnika" << std::endl;
  std::cout << "11 -> Azuriranje postojecih korisnika" << std::endl;
  std::cout << "12 -> Pregled posudjenih filmova odabranog korisnika" << std::endl;
  std::cout << "13 -> Pregled historije posudjivanja odabranog korisnika"<< std::endl;
  std::cout << "14 -> Izlaz iz programa" << std::endl;
  std::cout << std::string(32,'*') << std::endl;
  std::cout << "Vas izbor je:";
  std::cin >> izbor;

  return izbor;
}
int userMenu()
{
  int izbor;

  std::cout <<std::string(10,'*')<<"Admin opcije"<<std::string(10,'*') << std::endl;
  std::cout << " 1 -> Pregled sadrzaja videoteke" << std::endl;
  std::cout << " 2 -> Pretraga sadrzaja videoteke po nazivu filma" << std::endl;
  std::cout << " 3 -> Ispis svih podataka za odabrani film" << std::endl;
  std::cout << " 4 -> Posudjivanje filma sa datim nazivom" << std::endl;
  std::cout << " 5 -> Vracanje filma"<< std::endl;
  std::cout << " 6 -> Pregled svoje historije posudjivanje filmova" << std::endl;
  std::cout << " 7 -> Pregled trenutne liste posudjenih filmova" << std::endl;
  std::cout << " 8 -> Izlaz iz programa" << std::endl;
  std::cout << std::string(32,'*') << std::endl;
  std::cout << "Vas izbor je: ";
  std::cin >> izbor;
  return izbor;
}
int updateMenu()
{
  int izbor;
  std::cout <<std::string(10,'*')<<"Promjena korisnickih podataka"<< std::string(10,'*')<< std::endl;
  std::cout << "1 -> Promjena imena i prezimena" << std::endl;
  std::cout << "2 -> Promjena maticnog broja" << std::endl;
  std::cout <<" 3 -> Promjena username i password-a"<<std::endl;
  std::cout << std::string(49,'*') << std::endl;
  std::cout << "Vas izbor je: ";
  std::cin >> izbor;
  return izbor;
}
queue<std::string> StringSplit(std::string line, const char delimiter)
{
  queue<std::string> res;
  size_t pos;
  while(line != "#")
  {
    pos = line.find(delimiter);
    if(pos == std::string::npos)
    {
      res.push(line);
      break;
    }
    res.push(line.substr(0, pos));
    line = line.substr(pos+1);
  }
  return res;
}


void loadData(BstTree& users)//ucitavanje iz datoteke
{
std::ifstream in("users.txt");
if(in.is_open()){
std::string line;
while(getline(in, line))
{
std::string fName,lName,jmbg,usename,pass;
int numberOfCopies;
queue<std::string> strings = StringSplit(line);
fName = strings.front();
strings.pop();
lName = strings.front();
strings.pop();
name fullName(fName,lName); 
jmbg = strings.front();
strings.pop();
date newdate(strings.front());
strings.pop();
usename = strings.front();
strings.pop();
pass = strings.front();
strings.pop();
for(int i=0; i<pass.length(); i++)
{
 char sgn;
 sgn = pass[i];
 sgn = sgn-5;
 pass[i]=sgn;
}
numberOfCopies =stoi(strings.front());
strings.pop();
user newUser(fullName,jmbg,newdate,usename,pass,numberOfCopies);
while(strings.size()>numberOfCopies)
{
newUser.addMovieToHistory(strings.front());
strings.pop();
}
while(strings.size()>0)
{
newUser.loanMovie(strings.front());
strings.pop();
}
users.insert(newUser);
}
in.close();
}
else throw string("Nemoguce ucitati iz baze");
}

int getch()
{
 int ch;
 struct termios t_old, t_new;
 tcgetattr(STDIN_FILENO, &t_old);
 t_new = t_old;
 t_new.c_lflag &= ~(ICANON | ECHO);
 tcsetattr(STDIN_FILENO, TCSANOW, &t_new);
 ch = getchar();
 tcsetattr(STDIN_FILENO, TCSANOW, &t_old);
 return ch;
}
std::string getpass(const char *prompt, bool show_asterisk)//?????
{
  const char BACKSPACE=127;
  const char RETURN=10;
  std::string password;
  unsigned char ch=0;
  std::cout <<prompt;
  while((ch=getch())!=RETURN)
  {
   if(ch==BACKSPACE)
    {
     if(password.length()!=0)
      {
        if(show_asterisk)
         std::cout <<"\b \b";
          password.resize(password.length()-1);
      }
    }
    else
     {
       password+=ch;
       if(show_asterisk)
         std::cout <<'*';
     }
  }
  std::cout <<std::endl;
    return password;
}
void loadData(movie_store& movie){
  ifstream in("movie_store.txt");
  if(in.is_open()){
  movie.read(in);
  in.close();
  }
  else throw string("Nemoguce ucitati iz baze");
}
int updateMovie(){ //opcije za izmjenu podataka o filmu
  
  int izbor;
  std::cout <<std::string(10,'*')<<"Promjena podataka o filmu"<< std::string(10,'*')<< std::endl;
  std::cout << "1 -> Promjena broja kopija filma" << std::endl;
  std::cout << std::string(49,'*') << std::endl;
  std::cout << "Vas izbor je: ";
  std::cin >> izbor;
  return izbor;
}
