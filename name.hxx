#ifndef _NAME_HXX
#define _NAME_HXX
#include<iostream>

class name
{
  private:
    std::string _firstName;
    std::string _lastName;

  public:
    name()=default; //default konstruktor
    name(std::string firstName, std::string lastName); //od dva stringa pravi ime
    name(const name& other); //copy konstruktor
    name(name&& other); //move konstruktore
    ~name()=default; //destruktor
    name& operator=(const name& other);//operator dodjeljivanja vrijednosti
    name& operator=(name&& other);//move operator dodjeljivanja vrijednosti
  
    const  bool operator<(const name& b) const;// A<B, po prezimenu
    const bool operator>(const name& b) const; // A>B, po prezimenu
    const  bool operator==(const name& b) const;// A==B, ime == ime && prezime == prezime
    const bool operator!=(const name& b) const;// A!=B, ime != ime || prezime  != prezime
    
    void printNameInfo() const;//ispis imena i prezimene u formatu Ime Prezime
    void setFirstName(const std::string& fN){_firstName = fN;}// postaviti first name
    void setLastName(const std::string& lN){_lastName = lN;}//  postaviti last name
    const std::string& getFirstName() const {return _firstName;} // dobiti vrijednost first name
    const std::string& getLastName() const {return _lastName;} // dobiti vrijednost last name
    
    friend std::ostream& operator<<(std::ostream& out, const name& otherN); // operator za ispis formatiran ispis
    friend std::istream& operator>>(std::istream& in, name& otherN); // unos, napravljene provjere za unos podataka
 };
std::ostream& operator<<(std::ostream& out, const name& otherN)
{
  out<<otherN._firstName<<","<<otherN._lastName;

  return out;
}
std::istream& operator>>(std::istream& in, name& otherN)
{
  std::string prezime,ime;
  std::cout<<"Unesite ime: ";
  in>>ime;
  std::cout<<"Unesite prezime: ";
  in>>prezime;
  if(ime[0]>='a' && ime[0]<='z')
  {
    ime[0] = ime[0]-('a'-'A');
  }
  if(prezime[0]>='a' && prezime[0]<='z')
  {
    prezime[0] = prezime[0]-('a'-'A');
  }
  otherN._firstName=ime;
  otherN._lastName = prezime;

  return in;
}
name::name(std::string firstName, std::string lastName):_firstName(firstName), _lastName(lastName){}
name::name(const name& other):_firstName(other._firstName), _lastName(other._lastName){}
name::name(name&& other):_firstName(other._firstName), _lastName(other._lastName)
{
  other._firstName =" ";
  other._lastName =" ";
}
name& name::operator=(const name& other)
{
  if(this != &other)
  {
    _firstName = other._firstName;
    _lastName = other._lastName;
  }

  return *this;
}
name& name::operator=(name&& other)
{
  _firstName = other._firstName;
  _lastName = other._lastName;
  other._firstName = " ";
  other._lastName = " ";

  return *this;
}
const bool name::operator<(const name& b) const
{
  return _lastName < b._lastName;
}
const bool name::operator>(const name& b) const
{
  return _lastName > b._lastName;
}
const bool name::operator==(const name& b) const
{
  return ((_firstName == b._firstName) && (_lastName == b._lastName));
}

const bool name::operator!=(const name& b) const
{
  return ((_firstName != b._firstName) || (_lastName != b._lastName));
}
void name::printNameInfo() const
{
  std::cout <<_firstName<<" "<<_lastName;
}
#endif
