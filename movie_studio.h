#ifndef _MOVIE_STUDIO_H
#define _MOVIE_STUDIO_H
#include"person_name.h"
#include"list_people.h"
class movieStudio;
ostream& operator<<(ostream&,const movieStudio&);
ofstream& operator<<(ofstream&,const movieStudio&);
istream& operator>>(istream&,movieStudio&);
ifstream& operator>>(ifstream&,movieStudio&);
class movieStudio{
  private:
    person_name _director;
    listOfPeople _list;
    string _publishCompany;
  public:
    movieStudio()=default;//constructor
    const movieStudio& operator=(const movieStudio&);//assignment
    movieStudio(const movieStudio&);//copy constructor
    friend ostream& operator<<(ostream&,const movieStudio&);
    friend ofstream& operator<<(ofstream&,const movieStudio&);
    friend istream& operator>>(istream&,movieStudio&);
    friend ifstream& operator>>(ifstream&,movieStudio&);
    ~movieStudio()=default;
};

ifstream& operator>>(ifstream& in,movieStudio& studio){
  in>>studio._director;
  in>>studio._list;
  getline(in,studio._publishCompany,',');
  return in;
}

ofstream& operator<<(ofstream& out,const movieStudio& studio){
  out<<studio._director<<",";
  out<<studio._list;
  out<<studio._publishCompany;
  return out;
}

ostream& operator<<(ostream& out,const movieStudio& otherStudio){
  out<<"Direktor:"<<otherStudio._director<<std::endl;
  out<<otherStudio._list<<std::endl;
  out<<"Izdavacka kuca:"<<otherStudio._publishCompany;
  return out;
}

istream& operator>>(istream& in,movieStudio& otherStudio){
  std::cout<<"Unesite direktora filma"<<std::endl;
  in>>otherStudio._director;
  in>>otherStudio._list;
  std::cout<<"Unesite izdavacku kucu"<<std::endl;
  getline(in,otherStudio._publishCompany);
  return in;
}

movieStudio::movieStudio(const movieStudio& otherStudio):_director(otherStudio._director),_list(otherStudio._list),_publishCompany(otherStudio._publishCompany){}

const movieStudio& movieStudio::operator=(const movieStudio& otherStudio){
  _director=otherStudio._director;
  _list=otherStudio._list;
  _publishCompany=otherStudio._publishCompany;
  return *this;
}

#endif

