#ifndef _LIST_PEOPLE_H
#define _LIST_PEOPLE_H
#include"person_name.h"

class listOfPeople;
ostream& operator<<(ostream&,const listOfPeople&);
ofstream& operator<<(ofstream&,const listOfPeople&);
istream& operator>>(istream&,listOfPeople&);
ifstream& operator>>(ifstream&,listOfPeople&);

class listOfPeople{
  private:
    list<person_name> _producers;
    list<person_name> _writters;
    list<person_name> _actors;
  public:
    listOfPeople()=default;//constructor
   // void setLOP(string);
    listOfPeople(const listOfPeople&);//copy constructor
    const listOfPeople& operator=(const listOfPeople&);//assignment
    void print()const;//printa sve tri liste;
    list<person_name> cutNamesFromString(string str)const;
    istream& setLists(istream&,list<person_name>&);
    friend ostream& operator<<(ostream&,const listOfPeople&);
    friend ofstream& operator<<(ofstream&,const listOfPeople&);
    friend istream& operator>>(istream&,listOfPeople&);
    friend ifstream& operator>>(ifstream&,listOfPeople&);
    ~listOfPeople()=default;//destructor
};
//fja dobija listu producenata u strigu a onda ih odvoji u listu i vrati
list<person_name> listOfPeople::cutNamesFromString(string str)const{
  list<person_name> temp;
  person_name Name; 
  string temp_name;
  int pos=0,k=0,n;
  n=count(str.begin(),str.end(),',');
  for(int i=0;i<n;i++){
  pos = str.find_first_of(',');
  temp_name = str.substr(k, pos-k);
  k=pos;
  str.erase(pos,1);
  Name.setName(temp_name); 
  temp.push_back(Name);
  }
  return temp;
}

ifstream& operator>>(ifstream& in,listOfPeople& lOP){
  
  std::string str;
  
  getline(in,str,'#');
  lOP._producers=lOP.cutNamesFromString(str);

  getline(in,str,'#');
  lOP._writters=lOP.cutNamesFromString(str);


  getline(in,str,'#');
  lOP._actors=lOP.cutNamesFromString(str);

  return in;
}

ofstream& operator<<(ofstream& out,const listOfPeople& lOP){
    
    out<<lOP._producers;
    out<<"#";
    
    out<<lOP._writters;
    out<<"#";
    
    out<<lOP._actors;
    out<<"#";
    return out;
}

istream& listOfPeople::setLists(istream& in,list<person_name>& l){

  person_name Name;
  in>>Name;
  while(in){
    l.push_back(Name);
    in>>Name;
  }
  in.clear();
  fflush(stdin);
  return in;
}

istream& operator>>(istream& in,listOfPeople& otherLOP){
  
  std::cout<<"Unesite producente"<<std::endl;
  otherLOP.setLists(in,otherLOP._producers);
  
  std::cout<<"Unesite scenariste"<<std::endl;
  otherLOP.setLists(in,otherLOP._writters);
  
  std::cout<<"Unesite glumce"<<std::endl;
  otherLOP.setLists(in,otherLOP._actors); 
  
  return in;
}
void listOfPeople::print()const{

    std::cout<<"Lista producenata:";

    std::cout<<_producers;
    std::cout<<endl;
    std::cout<<"Lista scenarista:";

    std::cout<<_writters;
    std::cout<<endl;
    std::cout<<"Lista glumaca:";

    std::cout<<_actors;
}
ostream& operator<<(ostream& out,const listOfPeople& otherLOP){
  otherLOP.print();
  return out;
}
const listOfPeople& listOfPeople::operator=(const listOfPeople& otherLOP){
  _producers=otherLOP._producers;
  _writters=otherLOP._writters;
  _actors=otherLOP._actors;
  return *this;
}

listOfPeople::listOfPeople(const listOfPeople& otherLOP):_producers(otherLOP._producers),_writters(otherLOP._writters),_actors(otherLOP._actors){}

#endif
