#ifndef _BIN_TREE_H
#define _BIN_TREE_H

#include<iostream>

template <class elemType>
struct binaryTreeNode
{
	elemType info;
	binaryTreeNode<elemType> *llink;
	binaryTreeNode<elemType> *rlink;
	
	binaryTreeNode(const elemType & v) : info(v), llink(nullptr), rlink(nullptr){}
};

template <class elemType>
class binaryTree
{
	public:
		const binaryTree<elemType>& operator=(const binaryTree<elemType>&);
		const binaryTree<elemType>& operator=(binaryTree<elemType>&&);
		bool empty() const;
		void inorder() const;
		void preorder() const;
		void postorder() const;
    void printAllUsersInorder() const;
		int height() const;		
		binaryTree(const binaryTree<elemType>& otherTree);
		binaryTree(binaryTree<elemType>&& otherTree);
		binaryTree();
		~binaryTree();	
		virtual void insert(const elemType &) = 0;
		virtual bool search(const elemType &) const = 0;
		virtual void remove(const elemType & ) = 0;	
	protected:
		binaryTreeNode<elemType>*root;
	private:
		void copy(binaryTreeNode<elemType>* &copiedTreeRoot,binaryTreeNode<elemType>* otherTreeRoot);	
		void destroy(binaryTreeNode<elemType>* &p);
		void inorder(binaryTreeNode<elemType> *p) const;
		void preorder(binaryTreeNode<elemType> *p) const;
		void postorder(binaryTreeNode<elemType> *p) const;
    void printAllUsersInorder(binaryTreeNode<elemType> *p) const;
		int height(binaryTreeNode<elemType> *p) const;
		int max(int x, int y) const;		
};


template <class elemType>
bool binaryTree<elemType>::empty() const
{
	return (root == nullptr);
}

template <class elemType>
binaryTree<elemType>::binaryTree()
{
	root = nullptr;
}

template <class elemType>
void binaryTree<elemType>::inorder() const
{
    inorder(root);
}

template <class elemType>
void binaryTree<elemType>::preorder() const
{
	preorder(root);
}

template <class elemType>
void binaryTree<elemType>::postorder() const
{
	postorder(root);
}

template <class elemType>
int binaryTree<elemType>::height() const
{
	return height(root);
}

template <class elemType>
void binaryTree<elemType>::inorder(binaryTreeNode<elemType> *p) const
{
  if(p == nullptr)
  {
    return;
  }
  else
  {
    inorder(p->llink);
    std::cout<<p->info<<std::endl;
    inorder(p->rlink);
  }
}

template <class elemType>
void binaryTree<elemType>::preorder(binaryTreeNode<elemType> *p) const
{
    // IMPLEMENTIRATI
  if(p== nullptr)
  {
    return;
  }
  else
  {
  std::cout<<p->info<<std::endl;
  preorder(p->llink);
  preorder(p->rlink);
  }
}

template <class elemType>
void binaryTree<elemType>::postorder(binaryTreeNode<elemType> *p) const
{
    // IMPLEMENTIRATiI
    postorder(p->llink);
    postorder(p->rlink);
    std::cout<<p->info<<std::endl;
}

template <class elemType>
int binaryTree<elemType>::height(binaryTreeNode<elemType> *p) const
{
    // IMPLEMENTIRATI
}

template <class elemType>
int binaryTree<elemType>::max(int x, int y) const
{
	if (x >= y)
		return x;
	else
		return y;
}

template <class elemType>
void binaryTree<elemType>::copy(binaryTreeNode<elemType>* &copiedTreeRoot,binaryTreeNode<elemType>* otherTreeRoot)
{
	if (otherTreeRoot == nullptr)
		copiedTreeRoot = nullptr;
	else
	{
		copiedTreeRoot = new binaryTreeNode<elemType>; 
		copiedTreeRoot->info = otherTreeRoot->info;
		copy(copiedTreeRoot->llink, otherTreeRoot->llink);
		copy(copiedTreeRoot->rlink, otherTreeRoot->rlink);
	}
}

template <class elemType>
void binaryTree<elemType>::destroy(binaryTreeNode<elemType>* &p)
{
    if(p==nullptr)
    {
      return;
    }
    else
    {
      destroy(p->llink);
      destroy(p->rlink);
      delete p;
    }
}

template <class elemType>
binaryTree<elemType>::binaryTree(const binaryTree<elemType>& otherTree)
{
	if (otherTree.root == nullptr) 
		root = nullptr;
	else			
		copy(root, otherTree.root);
}

template <class elemType>
binaryTree<elemType>::binaryTree(binaryTree<elemType>&& otherTree)
{
    root = otherTree.root;
    otherTree.root = nullptr;
}

template <class elemType>
binaryTree<elemType>::~binaryTree()
{
	destroy(root);
}


template <class elemType>
const binaryTree<elemType>& binaryTree<elemType>::operator=(const binaryTree<elemType>& otherTree)
{
	if (this != &otherTree)
	{
		if (root != nullptr)
			destroy(root);
		if (otherTree.root == nullptr)
			root = nullptr;
		else
			copy(root, otherTree.root);
	}
    return *this;
}

template <class elemType>
const binaryTree<elemType>& binaryTree<elemType>::operator=(binaryTree<elemType>&& otherTree)
{
  if(root != nullptr)
    destroy(root);
  if(otherTree.root == nullptr)
  {
    root = nullptr;
  }
  else
  {
    root = otherTree.root;
    otherTree.root = nullptr;
  }
}
template<typename elemType>
void binaryTree<elemType>::printAllUsersInorder(binaryTreeNode<elemType> *p) const
{
  
  if(p == nullptr)
  {
    return;
  }
  else
  {
    printAllUsersInorder(p->llink);
    std::cout<<p->info<<std::endl;
    printAllUsersInorder(p->rlink);
  }
}

#endif
