#ifndef _LIST_H
#define _LIST_H
#include<iostream>
#include<iterator>
#include<stdexcept>
#include"node1.hxx"
#include"iterator.hxx"
#include"constIterator.hxx"
template <typename Type>
class list;//dodano

template<typename Type>
std::ostream& operator<<(std::ostream&,const list<Type>&);
template<typename Type>
std::ofstream& operator<<(std::ofstream&,const list<Type>&);

template <typename Type>
class list
{
    public:

using iterator1 = iterator<Type>;//greska.Bilo iterator
using const_iterator1 = const_iterator<Type>;//greska.Bilo const_iterator
        
        size_t size() const; 
        bool empty() const;         
        
       
        void clear();             
        
        void push_back(const Type& val); 
        void push_back(Type&& val);         
       
        void push_front(const Type& elem);
        void push_front(Type&& val);         
        void pop_back();
        void pop_front();
        void sendToBack();
        //Metod insert dodaje novi element iza proslijedjene pozicije vraca iterator na novu poziciju
        //Slozenost O(1)
        iterator1 insert(const_iterator1 position, const Type& val)
        {

        if(position.getPtr()== nullptr)
        {
        push_back(val);
        return iterator1(first);
        } 
        else
        {
         nodeType<Type>* current = new nodeType<Type>;
         current->setInfo(val);
         auto p_n = position.getPtr();
         current->setNext(p_n->getNext());
         p_n ->setNext(current);
         if(empty())
         last = current;
         p_n = current;
         count++;
    
         return iterator1(p_n);
        }
        }          

   
        iterator1 insert (const_iterator1 position, Type&& val)
        {
          if(position.getPtr()== nullptr)
          {
            push_back(val);
            return iterator1(first);
          }
          else
          {
            nodeType<Type>* current = new nodeType<Type>;
            current->setInfo(val);
            auto p_n = position.getPtr();
            current->setNext(p_n->getNext());
            p_n->setNext(current);
            if(empty())
            {
              last = current;
            }
	          count++;          
            p_n = current;
            return iterator1(p_n);

          }
          
  
        }      
        
        Type& front();       
        const Type& front() const;  
        Type& back(); 
        const Type& back() const;        
        
        iterator1 begin(){return iterator1(first);} 
        const_iterator1 begin( ) const  {return  const_iterator1(first);}        
        iterator1 end(){return iterator1(nullptr);} 
        const_iterator1 end() const{return const_iterator1(nullptr);} 
        void copyList(const list<Type>& x);
        
        list(); 
        list (const list<Type>& x);
        list (list<Type>&& x);        
        ~list(); 
        list<Type>& operator= (const list<Type>& x); 
        list<Type>& operator= (list<Type>&& x);
        friend std::ostream& operator<< <>(std::ostream&,const list&);
        friend std::ofstream& operator<< <>(std::ofstream&,const list<Type>&);
    private:
		size_t count; 
		nodeType<Type> *first; 
		nodeType<Type> *last; 
};

template<typename Type>
std::ofstream& operator<<(std::ofstream& out,const list<Type>& l){
  for(auto it=l.begin();it!=l.end();it++)
    out<<(*it)<<",";
  return out;
}

template<typename Type>
std::ostream& operator<<(std::ostream& out,const list<Type>& l){
  for(auto it=l.begin();it!=l.end();it++)
    out<<(*it)<<",";
  return out;
}

template<typename Type>
size_t list<Type>::size() const
{
  return count;
}
template<typename Type>
bool list<Type>::empty() const
{
  return count == 0;
}

template<typename Type>
void list<Type>::clear()
{
  nodeType<Type>* current = first;

  while(current !=nullptr)
  {
    first = current->getNext();
    delete current;
    current = first;
  }

  count =0;
  first = nullptr;
  last = nullptr;
}
template<typename Type>
void list<Type>::push_back(const Type& val)
{
  nodeType<Type>* current = new nodeType<Type>;
  current->setInfo(val);
  current->setNext(nullptr);
  if(!empty())
    last->setNext(current);
  else
    first = current;

  last = current;
  count++;
}

template<typename Type>
void list<Type>::push_back(Type&& val)
{
  nodeType<Type>* current = new nodeType<Type>;
  current->setInfo(val);
  current->setNext(nullptr);
  if(!empty())
    last->setNext(current);
  else
    first = current;

  last = current;
  count++;
}
template<typename Type>
void list<Type>::push_front(const Type& val)
{
  nodeType<Type>* current = new nodeType<Type>;
  current->setInfo(val);
  current->setNext(first);
  if(empty())
  last = current;
  

  first = current;
  count++;
}

template<typename Type>
void list<Type>::push_front( Type&& val)
{
  nodeType<Type>* current = new nodeType<Type>;
  current->setInfo(val);
  current->setNext(first);
  if(empty())
  last = current;
  

  first = current;
  count++;
}

template<typename Type>
void list<Type>::pop_back()
{
  if(empty())
    throw std::string("Lista je prazna!");
  else
  {
    if(count == 1)
    {
      delete first;
      first = nullptr;
      last = nullptr;
      count =0;
    }
    else
    {
      nodeType<Type>* current = first;
      while(current->getNext()!=last)
      {
        current = current->getNext();
      }
      delete last;
      current->setNext(nullptr);
      last = current;
      count--;
    }
  }
 }
template<typename Type>
void list<Type>::pop_front()
{
  if(empty())
    throw std::string("Lista je prazna");
  else
  {
    if(count == 1)
    {
      delete first;
      first = nullptr;
      last = nullptr;
      count = 0;
    }
    else
    {
      nodeType<Type>* current = first->getNext();
      delete first;
      first = current;
      count--;
    }
  }
 
}
template<typename Type>
Type& list<Type>::front()
{
  if(first == nullptr)
    throw std::string("Nema elementata u listi!");
  else
    return first->getInfo();
}

template<typename Type>
const Type& list<Type>::front() const
{
  if(empty())
    throw std::string("Nema elementata u listi!");
  else
    return first->getInfo();
}

template<typename Type>
Type& list<Type>::back()
{
  if(empty())
    throw std::string("Nema elemenata u listi");
  else
    return last->getInfo();
}

template<typename Type>
const Type& list<Type>::back() const
{
  if(empty())
    throw std::string("Nema elemenata u listi");
  else
    return last->getInfo();
}
template<typename Type>
list<Type>::list() : first{nullptr},last{nullptr},count{0}{}

template<typename Type>
list<Type>::list(const list<Type>& x)
{
first = nullptr;
copyList(x);
}
template<typename Type>
void list<Type>::copyList(const list<Type>& x)
{
  nodeType<Type>* newNode;
  nodeType<Type>* current;

  if(!empty())
    clear();

  if(x.first == nullptr)
  {
    first = nullptr;
    last = nullptr;
    count = 0;
  }
  else
  {
    current = x.first;
    count = x.count;
    first = new nodeType<Type>;
    first->setInfo(current->getInfo());
    first->setNext(nullptr);
    last = first;
    current = current->getNext();
    while(current != nullptr)
    {
      newNode = new nodeType<Type>;
      newNode->setInfo(current->getInfo());
      newNode->setNext(nullptr);
      last->setNext(newNode);
      last=newNode;//greska Bilo last->newNode
      current = current->getNext();
    }

  }
}
template<typename Type>
list<Type>::list(list<Type>&& x): first{x.first},last{x.last},count{x.count}
{
  x.first = nullptr;
  x.last = nullptr;
  x.count = 0;
}
template<typename Type>
list<Type>::~list()
{
  clear();
}
template<typename Type>
list<Type>& list<Type>::operator=(const list<Type>& x)
{
  if(this != &x)
    copyList(x);

  return *this;
}
template<typename Type>
list<Type>& list<Type>::operator=(list<Type>&& x)
{
  clear();
  first = x.first;
  count = x.count;
  last = x.last;
  x.first = nullptr;
  x.last = nullptr;
  x.count = 0;
  
}
template<typename Type>
void list<Type>::sendToBack()
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan!");
  }
  else if (size()>1)
  {
    nodeType<Type>* temp=first->getNext();
    last->setNext(first);
    first->setNext(nullptr);
    last = first;
    first = temp;
  }
}
  
#endif
