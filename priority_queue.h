#include <iostream>
#include <utility>
#include <algorithm>
#include <stdexcept>


template<typename T>
class priority_queue
{    
    public:
        priority_queue() = default;         
        priority_queue(const priority_queue<T>&);           
        priority_queue(priority_queue<T>&&); 
          
        ~priority_queue();

        priority_queue<T>& operator=(const priority_queue<T>&);            
        priority_queue<T>& operator=(priority_queue<T>&&); 

        int size() const;
        bool empty() const;
        const T& top();
        template<typename U>
        void push(U&&);
        void pop();
        T*const getPointer()const{return _el+1;}
    private:
        int _capacity=10;
        int _size=0;
        T *_el = new T[_capacity];
        void bottom_up();
        void top_down();
        void reallocate();
};


template<typename T>
priority_queue<T>::priority_queue(const priority_queue<T>& x) : _capacity(x._capacity), _size(x._size)
{   
    std::copy(x._el, x._el+_size+1, _el);
}

template<typename T>
priority_queue<T>::priority_queue(priority_queue<T>&& x) : _capacity(x._capacity), _size(x._size), _el(x._el)
{
    x._el = nullptr;
    x._size = 0;
}

template<typename T>
priority_queue<T>::~priority_queue()
{
    delete [] _el;
}

template<typename T>
priority_queue<T>& priority_queue<T>::operator=(const priority_queue<T>& x)
{
    if(this != &x)
    {  
        delete [] _el;
        _capacity = x._capacity;
        _size = x._size;
        _el = new T[_capacity];
        std::copy(x._el, x._el+_size+1, _el);
    }
    return *this;
}

template<typename T>
priority_queue<T>& priority_queue<T>::operator=(priority_queue<T>&& x)
{
    delete [] _el;
    _el = x._el;
    _capacity = x._capacity;
    _size = x._size;
    x._el = nullptr;
    x._size = 0;
    return *this;
}

template<typename T>
int priority_queue<T>::size() const
{
    return _size;
}

template<typename T>
bool priority_queue<T>::empty() const
{
    return _size == 0;
}

template<typename T>
const T& priority_queue<T>::top()
{
    if(empty())
    {
        throw std::out_of_range ("Queue is empty!");    
    }

    return _el[1];
}

template<typename T>
template<typename U>
void priority_queue<T>::push(U&& element)
{
    if(_size+1==_capacity) reallocate();
    _el[++_size] = std::forward<U>(element);   
    bottom_up();//uspostavljanje narusene gomile
}

template<typename T>
void priority_queue<T>::pop()
{
    if(empty())
    {
        throw std::out_of_range ("Queue is empty!");    
    }
    //zamijenimo prvi i zadnji cvor    
    std::swap(_el[1], _el[_size--]);
    top_down();//uspostaviti osobinu gomile    
}

template<typename T>
void priority_queue<T>::reallocate()
{
    _capacity=_capacity*2;
    T *newqueue = new T[_capacity];
    std::copy(_el, _el+_size+1, newqueue);
    delete [] _el;
    _el = newqueue;
}

template<typename T>
void priority_queue<T>::bottom_up()
{
    int k = _size;
    while (k > 1 && _el[k/2] < _el[k])
    {
        std::swap(_el[k], _el[k/2]);
        k = k/2;
    }
}

template<typename T>
void priority_queue<T>::top_down()
{
    int n = _size;
    int k = 1;
    
    while (2*k <= n)
    {
        int j = 2*k;// j je indek najveceg(po prioritetu)djeteta.prvo lijevog djeta
        if (j < n &&_el[j] < _el[j+1]) ++j;//ako nije lijevog onda desnog
        if (!(_el[k] < _el[j])) break;//ako najv dijet nije > od rodit break
        std::swap(_el[k], _el[j]);//u protivnom zamijeni roditelja sa najv dijetom
        k = j;//k=indeks bivseg najveceg djeteta
    }
}


  
