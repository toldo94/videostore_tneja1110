Videoteka videostore_tneja1110


Ovaj direktorji sadrži sve potrebne datoteke kako bi se implmentirao
program  koji prikazuje rad jedne videoteke. Sam rad videoteke može se
podijeliti na dvije zasebne velike cjeline. Jedna cjelina koja vodi računa
o korisnicima videoteke i druga cjelina koja vodi računa o filmovima koje
videoteka sadrži. Za svakog korisnika videoteke omogućen je pregled videoteke
pozajmljivanje vraćanje filmova dok admin ima neke dodatne privilegije. Svi
podaci o korisnicima i filmovima su sadržani u datotekama. Da bi aplikacija
ispravno radila neophodno je da postoje te dvije datoteke movie_store.txt i
users.txt. Prilikom pokretanja aplikacije u programu su postavljena dva admina
,nije moguće dodavati nove admine. Organizacija programa je urađena tako da
se omogući brzo i efikasno pretaživanje liste korsiniki i filmova.

U relizaciji ovog prejkata su učestvovali:

# Jakub Šečić - jakub.secic@fet.ba

Svojim radom omogućio sve opcije koje se odnose na rad sa filmovima,
pregled filmova, pretraživanje dodavanje novih itd.

# Nermin - nermin.hrustic@fet.ba

Svojim radom omogućio sve opcije koje se onose na rad sa korisnicima.
Pretraživanje korisnika, dodavanje novih, uklanjanje korisnika itd.

U narednom su navedeni svi koraci za kreiranje CMake file-a

BUILD process
- Navigate to project root directory in terminal where CMakeLists.txt file is at.
- Run following commands:
 mkdir build
 cd build
  cmake ..
  make
- These commands above should create executable "Zadaca",

run it with: ./Zadaca
