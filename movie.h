#ifndef _FILM_H
#define _FILM_H
#include<iostream>
#include<fstream>
#include<algorithm>
#include<string>
#include"lista.hxx"
#include"movie_studio.h"
using namespace std;
class movie;
ostream& operator<<(ostream& out,const movie& f);
ofstream& operator<<(ofstream& out,const movie& f);
ifstream& operator>>(ifstream& out,movie& f);
class movie{
  private:
    string _name;
    string _description;
    movieStudio _studio;
    int _relaseDate;
    int _numberOfCopies=0;
  public:
    movie()=default;
    const string& getName()const{return _name;}
    int getRelaseDate()const{return _relaseDate;}
    void set_numberOfCopies(){_numberOfCopies++;}
    void set_numberOfCopies(int n){_numberOfCopies=n;}
    int getNumberOfCopies(){return _numberOfCopies;}
    void unSet_numberOfCopies(){_numberOfCopies--;}
    bool operator==(const string&);
    const movie& operator=(const movie&);
    bool operator<(const movie&);
    movie(string,string,movieStudio,int,int);
    void print()const;
    friend ostream& operator<<(ostream& out,const movie& f);
    friend ofstream& operator<<(ofstream& out,const movie& f);
    friend ifstream& operator>>(ifstream& out,movie& f);
    ~movie()=default;
};

ifstream& operator>>(ifstream& in,movie& f){
  string temp;
  int broj;
  getline(in,f._name,',');
  getline(in,f._description,'#');
  in>>f._studio;
  getline(in,temp,',');
  f._relaseDate=(int)atoi(temp.c_str());
  getline(in,temp,',');
  f._numberOfCopies=(int)atoi(temp.c_str());
  getline(in,temp);
  return in;
}

ofstream& operator<<(ofstream& out,const movie& f){
  out<<f._name<<","<<f._description<<"#";
  out<<f._studio<<",";
  out<<f._relaseDate<<","<<f._numberOfCopies<<",";
  out<<std::endl;
  return out;
}

const movie& movie::operator=(const movie& f){
 _name=f._name;
 _description=f._description;
 _studio=f._studio;
 _relaseDate=f._relaseDate;
 _numberOfCopies=f._numberOfCopies;
}

movie::movie(string s1,string s2,movieStudio s3,int g,int k):_name(s1),_description(s2),_studio(s3),_relaseDate(g),_numberOfCopies(k){}//konstruktor
void movie::print()const{//ispis filma
  std::cout<<std::endl;
  std::cout << std::string(49,'*') << std::endl;
  std::cout<<"Naziv filma:"<<_name<<std::endl;
  std::cout<<"Zanr:"<<_description<<std::endl;
  std::cout<<_studio<<std::endl;
  std::cout<<"Godina izdavanja:"<<_relaseDate<<std::endl;
  std::cout<<"Broj kopija:"<<_numberOfCopies<<std::endl;
  std::cout << std::string(49,'*') << std::endl <<std::endl;
}

bool movie::operator==(const string& str){//pretraz po imenu
  if(this->_name==str)return true;
else return false;
}
bool movie::operator<(const movie& f){//rjesavanje prioriteta
  if(this->_relaseDate < f._relaseDate)return true;
  else return false;
}

ostream& operator<<(ostream& out,const movie& f){
  f.print();
  return out;
}
#endif

