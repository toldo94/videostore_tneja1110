#ifndef _BST_H
#define _BST_H
#include"user.hxx"
#include<iostream>
#include"binaryTree.hxx"
#include<cstdlib>
#include<fstream>
#include"name.hxx"
class BstTree: public binaryTree<user>
{
	public:
		bool search(const user& item) const;		
		void insert(const user& item);		
		void remove(const user& item);
    void printAllUsers(); //ispis svih korisnika
    void addNewUser();//dodavanje novog korisnika
    void deleteSpecificUser();//brisanje korisnka
    void searchForUser();//metod za pretragu korisnika
    void saveDatatoFile();//spremanje u datoteku
    void updateUser(int n);//promjena podataka o korisniku
    user& searchSpecificUser(std::string& username);//vraca korisnika
    bool isUserThrere(std::string& username) const; //provjerava da li postoji korsinik
    bool isUserCorrect(const std::string& username,const std::string& pass) const;
    void updateCurrentUser(user& currentUser);
	private:
		void removeNode(binaryTreeNode<user>* &p);
    void printAllUsersInorder(binaryTreeNode<user> *p);
    void saveUsersToFile(binaryTreeNode<user>* p, std::ofstream& outFile) const;

};

bool BstTree::search(const user& item) const
{
	binaryTreeNode<user> *current; 
	bool found = false;
	if (this->root == nullptr)  
		std::cout << "Cannot search the empty tree." << std::endl;
	else
	{
		current = this->root; 
		while (current != nullptr && !found) 
		{
			if (current->info == item) 
				found = true;
			else if (current->info > item) 			
				current = current->llink;
			else
				current = current->rlink; 
		}
	}
	return found;
}

void BstTree::insert(const user& item)
{
	binaryTreeNode<user> *current; 
	binaryTreeNode<user> *trailCurrent; 
	binaryTreeNode<user> *newNode; 
	newNode = new binaryTreeNode<user>(item);
	if (this->root == nullptr)
		this->root = newNode;
	else
	{
		current = this->root;
		while (current != nullptr)
		{
			trailCurrent = current;
			if (current->info.getUseranme() == item.getUseranme())
			{
        std::cout << "Takav korisnik već postoji!" << std::endl;
				return;
			}
			
			else if (current->info > item)
				current = current->llink;
			else 
				current = current->rlink;
		}
		if (trailCurrent->info > item) 
			trailCurrent->llink = newNode;
		else 
			trailCurrent->rlink = newNode;
	}
}

void BstTree::remove(const user& item)
{
	binaryTreeNode<user> *current;
	binaryTreeNode<user> *trailCurrent;
	bool found = false;
	if (this->root == nullptr)
		std::cout << "Niti jedan korisnik nije uclanjen." <<  std::endl;
	else
	{
		current = this->root;
		trailCurrent = this->root;
		while (current != nullptr && !found)
		{
			if (current->info == item)
				found = true;
			else
			{
				trailCurrent = current;
				if (current->info > item) 
					current = current->llink;				
				else
					current = current->rlink;
			}
		}
	if (current == nullptr)
		std::cout << "Takav korisnik nije uclanjen u videoteku." << std::endl;
	else if (found)
	{
		if (current == this->root)
			removeNode(this->root);		
		else if (trailCurrent->info > item) 
			removeNode(trailCurrent->llink);		
		else
			removeNode(trailCurrent->rlink);
	}
	}
}


void BstTree::removeNode(binaryTreeNode<user>* &p)
{
	
	binaryTreeNode<user> *current;
	binaryTreeNode<user> *trailCurrent; 
	binaryTreeNode<user> *temp;
	if (p == nullptr) 
		std::cout << "Error: The node to be deleted is nullptr." << std::endl;
	else if(p->llink == nullptr && p->rlink == nullptr)
	{
		temp = p; 
		p = nullptr;
		delete temp;
	}
	else if(p->llink == nullptr)
	{
		temp = p;
		p = temp->rlink;
		delete temp;
	}
	else if(p->rlink == nullptr)
	{
		temp = p;
		p = temp->llink;
		delete temp;
	}
	else
	{
		current = p->llink;
		trailCurrent = nullptr;
		while (current->rlink != nullptr)
		{
			trailCurrent = current;
			current = current->rlink;
		}
		p->info = current->info;
		if (trailCurrent == nullptr)		
			p->llink = current->llink;
		else 
			trailCurrent->rlink = current->llink;
		delete current;
	}
}
void BstTree::printAllUsersInorder(binaryTreeNode<user>* p)
{
  if(p == nullptr)
  {
    return;
  }
  else
  {
    printAllUsersInorder(p->llink);
    p->info.printUserData();
    printAllUsersInorder(p->rlink);
  }
}

void BstTree::saveUsersToFile(binaryTreeNode<user>* p,std::ofstream& outFile) const
{
  if(p == nullptr )
  {
    return;
  }
  else
  {
   outFile <<p->info <<"#"<< std::endl;
   saveUsersToFile(p->llink,outFile);
   saveUsersToFile(p->rlink,outFile);
  }
}

void BstTree::printAllUsers() 
{
  printAllUsersInorder(root);
}
void BstTree::addNewUser()
{
  user newUser;
  std::cin >>newUser;
  insert(newUser);
}
void BstTree::deleteSpecificUser()
{
  std::string username;
  std::cout << "Unesite username korisnika:";
  std::cin >> username;

	binaryTreeNode<user> *current;
	binaryTreeNode<user> *trailCurrent;
	bool found = false;
	if (this->root == nullptr)
		std::cout << "Takav korisnik ne postoji." <<  std::endl;
	else
	{
		current = this->root;
		trailCurrent = this->root;
		while (current != nullptr && !found)
		{
			if (current->info.getUseranme() == username)
				found = true;
			else
			{
				trailCurrent = current;
				if (current->info.getUseranme() > username) 
					current = current->llink;				
				else
					current = current->rlink;
			}
		}
	if (current == nullptr)
		std::cout << "Takav korisnik ne postji." << std::endl;
	else if (found)
	{
		if (current == this->root)
			removeNode(this->root);		
		else if (trailCurrent->info.getUseranme() > username) 
			removeNode(trailCurrent->llink);		
		else
			removeNode(trailCurrent->rlink);
	}
	}
}
void BstTree::searchForUser()
{
 std::string username;
  std::cout << "Unesite username: ";
  std::cin >> username;
   
	binaryTreeNode<user> *current; 
	bool found = false;
	if (this->root == nullptr)  
		std::cout << "Takav korisnik nije uclanjen u videoteku." << std::endl;
	else
	{
		current = this->root; 
		while (current != nullptr && !found) 
		{
			if (current->info.getUseranme() == username) 
				found = true;
			else if (current->info.getUseranme() > username) 			
				current = current->llink;
			else
				current = current->rlink; 
		}
	}
  if(found == true)
  {
    current->info.printUserData();
  }
  else
  {
    std::cout << "Takav korisnik nije uclanjen u videoteku!" << std::endl;
  }
}
void BstTree::saveDatatoFile()
{
  std::ofstream outFile;
  outFile.open("users.txt");
  saveUsersToFile(root,outFile);
}
void BstTree::updateUser(int n)
{
  
  std::string username;
  std::cout << "Unesite username: ";
  std::cin >> username;
   
	binaryTreeNode<user> *current; 
	bool found = false;
	if (this->root == nullptr)  
		std::cout << "Takav korisnik nije u videoteci." << std::endl;
	else
	{
		current = this->root; 
		while (current != nullptr && !found) 
		{
			if (current->info.getUseranme() == username) 
				found = true;
			else if (current->info.getUseranme() > username) 			
				current = current->llink;
			else
				current = current->rlink; 
		}
	}
  if(found == true)
  {
    switch(n)
    {
      case 1://ime i prezime
        {
          name newName;
          std::cin >> newName;
          current->info.setUserFullName(newName);
         break;
        }
      case 2:
        {
          std::string jmb;
          bool sign;
          do
          {
            sign = true;
            std::cout << "Unesite maticni broj: ";
            std::cin >> jmb;
            for(int i=0; i<jmb.length();i++)
              {
                if(!(jmb[i]>='0' && jmb[i]<='9') || jmb.length()!=13)
                    {
                      std::cout << "Neispravan unos maticnog broja. Ponovite unos! " << std::endl;
                      sign = false;
                      break;
                    }
              }
          }while(sign == false);
          current->info.setJMB(jmb);

          break;
        }
      case 3://username & password
        {
          bool sign;
          std::string username,password; 
            do
            {
              sign = true;
              std::cout <<"Unesite useraname: ";
              std::cin>>username;
              for(int i=0; i<username.length(); i++)
                {
                  if(!(username[i]>='a' && username[i]<='z')  && !(username[i]>='A' && username[i]<='Z') && !(username[i]>='0'&& username[i]<='9'))
                    {
                      std::cout << "Neispravno korisnicko ime. Ponovite unos!" << std::endl;
                      sign = false;
                      break;
                    }
                }
            }while(sign == false);

  do
  {
    sign = true;
    std::cout <<"Unesite password: ";
    std::cin>>password;
    if(password == username)
    {
      std::cout << "Password ne smije biti identican kao username. Ponovite unos!" << std::endl;
      sign = false;
    }
    for(int i=0; i<password.length(); i++)
    {
    if(!(password[i]>'a'&& password[i]<'z') && !(password[i]>'A'&& password[i]<'Z')&&!(password[i]<'0'&& password[i]>'9') && password.length()<8)
    {
        std::cout << "Passwor nije validan. Ponovite unos!" << std::endl;
        sign = false;
        break;
      }
      }
    }while(sign == false);
              
            current->info.setUsername(username);
            current->info.setPassword(password);
          break;
          }
        default:
          {
            std::cout<<"Ta opcija ne postoji"<<std::endl;
          }
      }
      
  }
    else
    {
      std::cout << "Takav korisnik nije uclanjen u videoteku!" << std::endl;
    }
  }
  user& BstTree::searchSpecificUser(std::string& username)
  {
  
    binaryTreeNode<user> *current; 
    bool found = false;
    if (this->root == nullptr)  
      std::cout << "Cannot search the empty tree." << std::endl;
    else
    {
      current = this->root; 
      while (current != nullptr && !found) 
      {
        if (current->info.getUseranme() == username) 
          found = true;
        else if (current->info.getUseranme() > username) 			
          current = current->llink;
        else
          current = current->rlink; 
      }
    }
   return current->info;
  }
  bool BstTree::isUserThrere(std::string& username) const
  {
    binaryTreeNode<user> *current; 
    bool found = false;
    if (this->root == nullptr)  
      std::cout << "Cannot search the empty tree." << std::endl;
    else
    {
      current = this->root; 
      while (current != nullptr && !found) 
      {
        if (current->info.getUseranme() == username) 
          found = true;
        else if (current->info.getUseranme() > username) 			
          current = current->llink;
        else
          current = current->rlink; 
      }
    }
    if(found == true)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  bool BstTree::isUserCorrect(const std::string& username,const std::string& pass) const
  {
    binaryTreeNode<user> *current; 
    bool found = false;
    if (this->root == nullptr)  
      std::cout << "Cannot search the empty tree." << std::endl;
    else
    {
      current = this->root; 
      while (current != nullptr && !found) 
      {
        if ((current->info.getUseranme() == username) && (current->info.getPassword() == pass))
          found = true;
        else if (current->info.getUseranme() > username) 			
          current = current->llink;
			else
				current = current->rlink; 
		}
	}
  if(found == true)
  {
    return true;
  }
  else
  {
     return false;
  }
}
void BstTree::updateCurrentUser(user& currentUser)
{
  
    binaryTreeNode<user> *current; 
    bool found = false;
    if (this->root == nullptr)  
      std::cout << "Cannot search the empty tree." << std::endl;
    else
    {
      current = this->root; 
      while (current != nullptr && !found) 
      {
        if ((current->info.getUseranme() == currentUser.getUseranme()) && (current->info.getPassword() == currentUser.getPassword()))
          found = true;
        else if (current->info.getUseranme() > currentUser.getUseranme()) 			
          current = current->llink;
			else
				current = current->rlink; 
		}
	}
    if(found == true)
    {
      current->info = currentUser;
    }
}
#endif
