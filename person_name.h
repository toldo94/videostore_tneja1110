#ifndef _NAME_H
#define _NAME_H
#include"lista.hxx"
using namespace std;//ukloniti
class person_name;
std::istream& operator>>(std::istream&,person_name&);
std::ostream& operator<<(std::ostream&,const person_name&);
std::ofstream& operator<<(std::ofstream&,const person_name&);
std::ifstream& operator>>(std::ifstream&,person_name&);
class person_name{
  private:
    std::string _firstName;
    std::string _lastName;
  public:
    person_name()=default;//default constructor
    person_name(std::string,std::string);//constructor
    person_name(const person_name&);//copy constructor
    person_name(person_name&&);//move constructor
    void setName(std::string);
    const person_name& operator=(const person_name&);//assignment
    const person_name& operator=(person_name&&);//move assignment
    friend std::istream& operator>>(std::istream&,person_name&);
    friend std::ostream& operator<<(std::ostream&,const person_name&);
    friend std::ofstream& operator<<(std::ofstream&,const person_name&);
    friend std::ifstream& operator>>(std::ifstream&,person_name&);
    ~person_name()=default;//default constructor
};
void person_name::setName(string str){
  int pos=str.find_first_of(' ');
  _lastName=str.substr(pos+1);
  _firstName=str.substr(0,pos);
}

std::ifstream& operator>>(std::ifstream& in,person_name& Name){
  getline(in,Name._firstName,' ');
  getline(in,Name._lastName,',');
  return in;
}

std::ofstream& operator<<(std::ofstream& out,const person_name& otherName){
  out<<otherName._firstName<<" "<<otherName._lastName;
  return out;
}

const person_name& person_name::operator=(person_name&& otherName){ 
  _firstName=otherName._firstName;
  _lastName=otherName._lastName;
  otherName._firstName=" ";
  otherName._lastName=" ";
}

const person_name& person_name::operator=(const person_name& otherName){
  _firstName=otherName._firstName;
  _lastName=otherName._lastName;
}

std::ostream& operator<<(std::ostream& out,const person_name& otherName){
  out<<otherName._firstName<<" "<<otherName._lastName;
  return out;
}

person_name::person_name(person_name&& Name):_firstName(Name._firstName),_lastName(Name._lastName){
  Name._firstName="";
  Name._lastName="";
}

person_name::person_name(const person_name& Name):_firstName(Name._firstName),_lastName(Name._lastName){}

person_name::person_name(std::string firstName,std::string lastName):_firstName(firstName),_lastName(lastName){}

std::istream& operator>>(std::istream& in,person_name& Name){
  std::cout<<"Unesite ime "<<std::endl;
  in>>Name._firstName;
  in.ignore();
  std::cout<<"Unesite prezime "<<std::endl;
  in>>Name._lastName;
  in.ignore();
  return in;  
}

#endif 
