#ifndef _QUEUE_HXX
#define _QUEUE_HXX
#include<iostream>
//#include"node.hxx"
#include<stdexcept>
#include"queue_interface.h"
template <typename Type> 
class Node
{ 
	private: 
		Type info; 
		Node* next; 
	public: 
    Node()=default;
    Node(const Type& i, Node* l = nullptr): info{i}, next{l}{};
		Type &getInfo() {return info;} 
		Node* getNext() {return next;} 
		void setInfo(const Type& i) {info=i;} 
		void setNext(Node* n) {next = n;} 
};
template<typename Type>
class queue : public  queue_interface<Type>
{

  public:
    queue();
    queue(const queue& other);
    queue(queue&& other);
    queue& operator=(const queue& other);
    queue& operator=(queue&& other);
    ~queue(){clearQueue();}
    void push(const Type& val);
    void push(Type&& val); 
    const Type& front() const;
    Type& front();
    const Type& back() const;
    Type& back();
    void pop();
    size_t size() const;
    bool empty() const;
    void sendToBack();
    void print() const;

  private:
    Node<Type>* first;
    Node<Type>* last;
    size_t count;
    void clearQueue();
    void copyQueue(const queue<Type>& otherQueue);

};
template<typename Type>
size_t queue<Type>::size() const 
{
  return count;
}
template<typename Type>
bool queue<Type>::empty() const 
{
  return (count == 0);
}
template<typename Type>
queue<Type>::queue()
{
  first = nullptr;
  last = nullptr;
  count = 0;
}
template<typename Type>
queue<Type>::queue(const queue<Type>& other)
{
  first = nullptr;
  last = nullptr;
  copyQueue(other);
}
template<typename Type>
void queue<Type>::copyQueue(const queue<Type>& otherQueue)
{
  Node<Type>* current;
  if(!empty())
    clearQueue();

  current = otherQueue.first;
  while(current!=nullptr)
  {
    push(current->getInfo());
    current = current->getNext();
  }

}
template<typename Type>
queue<Type>::queue(queue<Type>&& other): first{other.first}, last{other.last}, count{other.count}
{
  other.first = nullptr;
  other.last = nullptr;
  other.count = 0;
}
template<typename Type>
queue<Type>& queue<Type>::operator=(const queue<Type>& other)
{
  if(this != &other)
  copyQueue(other);

  return *this;
}
template<typename Type>
queue<Type>& queue<Type>::operator=(queue<Type>&& other)
{
  clearQueue();
  first = other.first;
  last = other.last;
  count = other.count;
  other.first = nullptr;
  other.last = nullptr;
  other.count = 0;

  return *this;
}
template<typename Type>
void queue<Type>::push(const Type& val)
{
  Node<Type>* current = new Node<Type>(val);
 if(!empty())
  {
  last->setNext(current);
  }
  else
  {
    first = current;    
  }
  last = current;
  count++;
}
template<typename Type>
void queue<Type>::push( Type&& val)
{
  Node<Type>* current = new Node<Type>(val);
 if(!empty())
  {
  last->setNext(current);
  }
  else
  {
    first = current;    
  }
  last = current;
  count++;
}
template<typename Type>
const Type& queue<Type>::front() const
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan");
  }
  else
  {
    return first->getInfo();
  }
}
template<typename Type>
Type& queue<Type>::front()
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan!");
  }
  else
  {
  return first->getInfo();
  }
}
template<typename Type>
const Type& queue<Type>::back() const
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan!");
  }
  else
  {
    return last->getInfo();
  }
}
template<typename Type>
Type& queue<Type>::back()
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan!");
  }
  else
  {
    return last->getInfo();
  }
}
template<typename Type>
void queue<Type>::pop()
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan!");
  }
  else
  {
  Node<Type>* temp = first;
  first = first->getNext();
  delete temp;
  if(first == nullptr)
  {
    last=nullptr;
  }

  count--;
  }
}
template<typename Type>
void queue<Type>::clearQueue()
{
  Node<Type>* current = first;
    while(current != nullptr)
    {
      first= first->getNext();
      delete current;
      current = first;
    }
    last = nullptr;
    count = 0;
   
}
template<typename Type>
void queue<Type>::sendToBack()
{
  if(empty())
  {
    throw std::invalid_argument("Red je prazan!");
  }
  else if(size()>1)
  {
    Node<Type>* temp=first->getNext();
    last->setNext(first);
    first->setNext(nullptr);
    last = first;
    first = temp;
  }
}
template<typename Type>
void queue<Type>::print() const
{
  Node<Type>* current = first;

  while(current != nullptr)
  {
    std::cout << current->getInfo() << std::endl;
    current = current->getNext();
  }

}
#endif

