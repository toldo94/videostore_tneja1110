#ifndef _USER_HXX
#define _USER_HXX
#include<iostream>
#include"date.hxx"
#include"name.hxx"
#include"lista.hxx"

class user 
{
  private:
    name _userFullName;
    std::string _jmb;
    date _signUpDate;
    std::string _username;
    std::string _password;
    list<std::string> _history;
    list<std::string> _loanedMovies;
    int _noOfMovies=0;


  public:
    user()=default;
    user(name& fN, std::string jmb, date& sUpDate, std::string uN, std::string pW,int noMov);
    user(const user& other);
    user(user&& other);
    user& operator=(const user& other);
    user& operator=(user&& other);

    void setUserFullName(const name& uFN) {_userFullName = uFN;}
    void setJMB(const std::string& jmb) {_jmb = jmb;}
    void setSignUpDate(const date& sUpDate) {_signUpDate = sUpDate;}
    void setUsername (const std::string uN) {_username = uN;}
    void setPassword(const std::string pW) {_password = pW;}
    const name& getUserFullName() const {return _userFullName;}
    const std::string& getJMB() const {return _jmb;}
    const date& getSingUpDate() const {return _signUpDate;}
    const std::string& getUseranme() const {return _username;}
    const std::string& getPassword() const {return _password;}
    const std::string& getFirstName() const {return _userFullName.getFirstName();}
    const std::string& getLastName() const {return _userFullName.getLastName();}

    const bool operator<(const user& b) const;
    const bool operator>(const user& b) const;
    const bool operator==(const user& b) const;
    const bool operator!=(const user& b) const;

    void addMovieToHistory(std::string nameAndYear); //dodavanje novih filmova u historiju
    void addMovieToHistory(std::string name, int year); // dodavanje novih filmova u historiju
    void printUserData() const;
    void loanMovie(std::string nameAndYear);
    void loanMovie(std::string name, int year);
    void printUserHistory() const;
    void printLoanedMovies() const;
    void returnMovie(std::string movieName);
 
    friend std::ostream& operator<<(std::ostream& out, user&  User);
    friend std::istream& operator>>(std::istream& in, user& User);
};
std::ostream& operator<<(std::ostream& out, user& User)
{
  out<<User._userFullName<<","<<User._jmb<<","<<User._signUpDate<<","<<User._username<<",";
  std::string password = User._password;
  for(int i=0;i<password.length(); i++)
  {
    char sign;
    sign = password[i];
    sign = sign + 5;
    password[i] = sign;
  }
  out<<password<<","<<User._noOfMovies<<",";
  for(auto it = User._history.begin(); it!=User._history.end();it++)
  {
    out<<*it<<",";
  }
  for(auto it = User._loanedMovies.begin(); it!=User._loanedMovies.end(); it++)
  {
    out<<*it<<",";
  }

  return out;
}
std::istream& operator>>(std::istream& in, user& User)
{
  name userFullName;
  std::string jmb;
  date signUpDate;
  std::string username;
  std::string password;
  bool sign;
  in>>userFullName;
  do
  {
    sign = true;
    std::cout << "Unesite maticni broj: ";
    in>>jmb;

    for(int i=0; i<jmb.length();i++)
    {
      if(!(jmb[i]>='0' && jmb[i]<='9') || jmb.length()!=13)
      {
        std::cout << "Neispravan unos maticnog broja. Ponovite unos! " << std::endl;
        sign = false;
        break;
      }
    }
  }while(sign == false);

  in>>signUpDate;

  do
  {
    sign = true;
    std::cout <<"Unesite useraname: ";
    in>>username;
    for(int i=0; i<username.length(); i++)
    {
      if(!(username[i]>='a' && username[i]<='z')  && !(username[i]>='A' && username[i]<='Z') && !(username[i]>='0'&& username[i]<='9'))
      {
        std::cout << "Neispravno korisnicko ime. Ponovite unos!" << std::endl;
        sign = false;
        break;
      }
    }
  }while(sign == false);

  do
  {
    sign = true;
    std::cout <<"Unesite password: ";
    in>>password;
    if(password == username)
    {
      std::cout << "Password ne smije biti identican kao username. Ponovite unos!" << std::endl;
      sign = false;
    }
    for(int i=0; i<password.length(); i++)
    {
    if(!(password[i]>'a'&& password[i]<'z') && !(password[i]>'A'&& password[i]<'Z')&&!(password[i]<'0'&& password[i]>'9') && password.length()<8)
    {
      std::cout << "Passwor nije validan. Ponovite unos!" << std::endl;
      sign = false;
      break;
    }
    }
  }while(sign == false);

  User._userFullName = userFullName;
  User._jmb = jmb;
  User._signUpDate = signUpDate;
  User._username = username;
  User._password = password;

  return in;
}
user::user(name& fN, std::string jmb, date& sUpDate, std::string uN, std::string pW, int noMov):_userFullName(fN),_jmb(jmb),_signUpDate(sUpDate),
  _username(uN),_password(pW),_noOfMovies(noMov), _history(){}
user::user(const user& other):_userFullName(other._userFullName), _jmb(other._jmb), _signUpDate(other._signUpDate),
  _username(other._username), _password(other._password),_noOfMovies(other._noOfMovies),_history(other._history),_loanedMovies(other._loanedMovies){}

user::user(user&& other)
{
  _userFullName = other._userFullName;
  _jmb = other._jmb;
  _signUpDate = other._signUpDate;
  _username = other._username;
  _password = other._password;
  _history = other._history;
  _noOfMovies = other._noOfMovies;
  _loanedMovies = other._loanedMovies;
}
user& user::operator=(const user& other)
{
  if(this != &other)
  {
    _userFullName = other._userFullName;
    _jmb = other._jmb;
    _signUpDate = other._signUpDate;
    _username = other._username;
    _password = other._password;
    _noOfMovies = other._noOfMovies;
    _history = other._history;
    _loanedMovies = other._loanedMovies;
  }

  return *this;
}
user& user::operator=(user&& other)
{
  _userFullName = other._userFullName;
  _jmb = other._jmb;
  _signUpDate = other._signUpDate;
  _username = other._username;
  _password = other._password;
  _noOfMovies = other._noOfMovies;
  _history= other._history;
  _loanedMovies = other._loanedMovies;

  return *this;
}
const bool user::operator<(const user& b) const
{
  return (_username < b._username);
}
const bool user::operator>(const user& b) const
{
  return (_username > b._username);
}
const bool user::operator==(const user& b) const
{
  return((_password == b._password) && (_username == b._username));
}
const bool user::operator!=(const user& b) const
{
  return((_password != b._password) || (_username != b._username));
}
void user::addMovieToHistory(std::string nameAndYear)
{
  _history.push_back(nameAndYear);
}
void user::addMovieToHistory(std::string name, int year)
{
  std::string nameAndYear = name+std::to_string(year);
  _history.push_back(nameAndYear);
}
void user::printUserData() const
{
  _userFullName.printNameInfo();
  std::cout <<" "<<_username<<" "<<_signUpDate<<std::endl;
}
void user::loanMovie(std::string name, int year)
{
  std::string nameAndYear = name+std::to_string(year);
  if(_noOfMovies<3 && nameAndYear != "")
  {
  _loanedMovies.push_back(nameAndYear);
  _noOfMovies=_loanedMovies.size();
  }
  else
  {
    std::cout <<"Trenutno imate 3 posudjena filma! Prvo vratite neki od filmova da biste mogli posuditi novi!"  << std::endl;
  }
}
void user::loanMovie(std::string nameAndYear)
{
  if(_noOfMovies<3 && nameAndYear != "")
  {
  _loanedMovies.push_back(nameAndYear);
  _noOfMovies=_loanedMovies.size();
  }
  else
  {
    std::cout <<"Trenutno imate 3 posudjena filma! Prvo vratite neki od filmova da biste mogli posuditi novi!"  << std::endl;
  }
}
void user::printLoanedMovies() const
{
  std::cout << "Trenutno pozajmljeni filmovi" << std::endl;
  if(_loanedMovies.empty())
  {
    std::cout << "Trenutno nemate niti jedan pozajmljen film." << std::endl;
  }
  else
  {
  for (auto it=_loanedMovies.begin(); it!=_loanedMovies.end(); it++)
  {
    std::cout <<*it<<std::endl;
  }
  }
}

void user::printUserHistory() const
{
  std::cout << "Historija posudjivanja filmova korisnika" << std::endl;
  if(_history.empty())
      std::cout << "Nikada niste posudili film" << std::endl;
      else
      for(auto it=_history.begin(); it!=_history.end(); it++)
      {
        std::cout << *it << std::endl;
      }
}
void user::returnMovie(std::string movieName)
{
  if(_loanedMovies.empty())
  {
    std::cout << "Trenutno nemate pozajmljenih filmova" << std::endl;
  }
  else
  {
  if(movieName == _loanedMovies.front())
  {
    addMovieToHistory(_loanedMovies.front());
    _loanedMovies.pop_front();
  }
  else if (movieName == _loanedMovies.back())
  {
    addMovieToHistory(_loanedMovies.back());
      _loanedMovies.pop_back();
  }
  else
  {
    _history.sendToBack();
    if(movieName == _loanedMovies.front())
    {
      addMovieToHistory(_loanedMovies.front());
      _loanedMovies.pop_front();
      _loanedMovies.sendToBack();
    }
    else
    {
      _loanedMovies.sendToBack();
      _loanedMovies.sendToBack();
      std::cout << "Nemate pozajmljen takav film" << std::endl;
    }
  }
  }
  _noOfMovies = _loanedMovies.size();//dodano
}
  #endif
